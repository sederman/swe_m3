package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datamanagement.DataManager;
import model.Flight;

/**
 * Servlet implementation class AddFlightServlet
 */
@WebServlet("/AddFlight")
public class AddFlightServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddFlightServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
boolean successful = false;
		
		
		String airline = request.getParameter("airline");
		String date = request.getParameter("date");
		String destination = request.getParameter("destination");
		String price = request.getParameter("price");
		String source = request.getParameter("source");
		String time = request.getParameter("time");
		String capacity = request.getParameter("capacity");
		
		Flight flight = new Flight();
		flight.setAirline(airline);
		flight.setDate(date);
		flight.setDestination(destination);
		flight.setPrice(Double.parseDouble(price));
		flight.setSource(source);
		flight.setTime(time);
		flight.setCapacity(Integer.parseInt(capacity));
		flight.setSeatsAvailable(Integer.parseInt(capacity));
			
		successful = DataManager.addFlightSuccess(flight);
		
		if(successful) {
			response.sendRedirect("flightAddSuccessful.jsp");
		}
		else response.sendRedirect("flightAddNotSuccessful.jsp");
	}

}
